Docker Swarm Deployment
=======================

This Ansible Role was created from the original Playbook at:
* https://github.com/nextrevision/ansible-swarm-playbook

The purpose of this Role is to set up a Docker Swarm cluster including
Swarm Manager redundancy and a bunch of Swarm Workers.

Requirements
------------

Python modules required in your Ansible environment:
* `docker` for the custom library module inherited from the original Playbook
* `molecule` for testing the role

Role Variables
--------------

A description of the settable variables for this role should go here, including
any variables that are in defaults/main.yml, vars/main.yml, and any variables
that can/should be set via parameters to the role. Any variables that are read
from other roles and/or the global scope (ie. hostvars, group vars, etc.) should
be mentioned here as well.
* `swarm_iface` in `defaults/main.yml` is essential to determine the
  Swarm communications interface
* `manager_join_token` in `vars/main.yml` is automatically optained
  through the Docker Info Module
* `worker_join_token` in `vars/main.yml` is automatically optained
  through the Docker Info Module

Dependencies
------------

* This role requires Docker to be installed on the host systems
* For a test environment you may use Miniconda to initialize it via the
  `environment.yml` file
* You need to define the groups `swarm_managers` and `swarm_workers` in
  your inventory

Example Playbook
----------------

See example playbook in `molecule/default/playbook.yml`

Testing environment
-------------------

* Install Miniconda
* `conda env create -f environment.yml`
* alternative update via `conda env update -f environment.yml`
* `source activate ansible-role`
* `molecule lint`
* `molecule check`
* `molecule verify`

License
-------

MIT License

Copyright (c) 2018 Alexander Kuemmel

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
