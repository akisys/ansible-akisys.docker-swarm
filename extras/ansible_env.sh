#!/usr/bin/env bash

#set -fuxe
set -fue

SCRIPT_DIR=$(cd `dirname $0` && pwd)
source "${SCRIPT_DIR}/project.env"

USER_CMD=${1:-''}

cd $PROJECT_DIR

case "${USER_CMD}" in
  install)
    conda env create
    ;;
  update)
    conda env update
    ;;
  *)
    (>&2 echo "Unknown option from available set of [install|update]")
    exit 1
    ;;
esac

